const SharedRouter = [
  {
    path:     '/',
    redirect: '/sample/sample1',
  },
  {
    path:      '/404',
    component: () => import('@/app/system/error/404.vue'),
  },
  {
    path:     '*',
    redirect: '/404',
  }
];

export default SharedRouter;
