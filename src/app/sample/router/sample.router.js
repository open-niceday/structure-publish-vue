const SampleRouter = [
  {
    path:      '/sample',
    component: () => import ('@/app/system/layout/default.vue'),
    redirect:  '/sample/sample1',
    children:  [
      {
        path:      'sample1',
        component: () => import('@/app/sample/view/sample1.vue'),
      },
      {
        path:      'sample2',
        component: () => import('@/app/sample/view/sample2.vue'),
      }
    ]
  }
];

export default SampleRouter;
